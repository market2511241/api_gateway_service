// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: smena.proto

package sale_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Smena struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	StaffId    string  `protobuf:"bytes,2,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchId   string  `protobuf:"bytes,3,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	SaleAmount float32 `protobuf:"fixed32,4,opt,name=sale_amount,json=saleAmount,proto3" json:"sale_amount,omitempty"`
	Status     string  `protobuf:"bytes,5,opt,name=status,proto3" json:"status,omitempty"`
	CreatedAt  string  `protobuf:"bytes,6,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	FinishedAt string  `protobuf:"bytes,7,opt,name=finished_at,json=finishedAt,proto3" json:"finished_at,omitempty"`
}

func (x *Smena) Reset() {
	*x = Smena{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Smena) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Smena) ProtoMessage() {}

func (x *Smena) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Smena.ProtoReflect.Descriptor instead.
func (*Smena) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{0}
}

func (x *Smena) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Smena) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *Smena) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Smena) GetSaleAmount() float32 {
	if x != nil {
		return x.SaleAmount
	}
	return 0
}

func (x *Smena) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *Smena) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Smena) GetFinishedAt() string {
	if x != nil {
		return x.FinishedAt
	}
	return ""
}

type SmenaCreateReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StaffId    string  `protobuf:"bytes,1,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchId   string  `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	SaleAmount float32 `protobuf:"fixed32,3,opt,name=sale_amount,json=saleAmount,proto3" json:"sale_amount,omitempty"`
	Status     string  `protobuf:"bytes,4,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SmenaCreateReq) Reset() {
	*x = SmenaCreateReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaCreateReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaCreateReq) ProtoMessage() {}

func (x *SmenaCreateReq) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaCreateReq.ProtoReflect.Descriptor instead.
func (*SmenaCreateReq) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{1}
}

func (x *SmenaCreateReq) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *SmenaCreateReq) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SmenaCreateReq) GetSaleAmount() float32 {
	if x != nil {
		return x.SaleAmount
	}
	return 0
}

func (x *SmenaCreateReq) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SmenaCreateResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
}

func (x *SmenaCreateResp) Reset() {
	*x = SmenaCreateResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaCreateResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaCreateResp) ProtoMessage() {}

func (x *SmenaCreateResp) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaCreateResp.ProtoReflect.Descriptor instead.
func (*SmenaCreateResp) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{2}
}

func (x *SmenaCreateResp) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

type SmenaGetListReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page  int64 `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Limit int64 `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
}

func (x *SmenaGetListReq) Reset() {
	*x = SmenaGetListReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaGetListReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaGetListReq) ProtoMessage() {}

func (x *SmenaGetListReq) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaGetListReq.ProtoReflect.Descriptor instead.
func (*SmenaGetListReq) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{3}
}

func (x *SmenaGetListReq) GetPage() int64 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *SmenaGetListReq) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

type SmenaGetListResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Smenas []*Smena `protobuf:"bytes,1,rep,name=smenas,proto3" json:"smenas,omitempty"`
	Count  int64    `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *SmenaGetListResp) Reset() {
	*x = SmenaGetListResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaGetListResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaGetListResp) ProtoMessage() {}

func (x *SmenaGetListResp) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaGetListResp.ProtoReflect.Descriptor instead.
func (*SmenaGetListResp) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{4}
}

func (x *SmenaGetListResp) GetSmenas() []*Smena {
	if x != nil {
		return x.Smenas
	}
	return nil
}

func (x *SmenaGetListResp) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

type SmenaIdReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *SmenaIdReq) Reset() {
	*x = SmenaIdReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaIdReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaIdReq) ProtoMessage() {}

func (x *SmenaIdReq) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaIdReq.ProtoReflect.Descriptor instead.
func (*SmenaIdReq) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{5}
}

func (x *SmenaIdReq) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type SmenaUpdateReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	StaffId    string  `protobuf:"bytes,2,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchId   string  `protobuf:"bytes,3,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
	SaleAmount float32 `protobuf:"fixed32,4,opt,name=sale_amount,json=saleAmount,proto3" json:"sale_amount,omitempty"`
	Status     string  `protobuf:"bytes,5,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *SmenaUpdateReq) Reset() {
	*x = SmenaUpdateReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaUpdateReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaUpdateReq) ProtoMessage() {}

func (x *SmenaUpdateReq) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaUpdateReq.ProtoReflect.Descriptor instead.
func (*SmenaUpdateReq) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{6}
}

func (x *SmenaUpdateReq) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *SmenaUpdateReq) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *SmenaUpdateReq) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *SmenaUpdateReq) GetSaleAmount() float32 {
	if x != nil {
		return x.SaleAmount
	}
	return 0
}

func (x *SmenaUpdateReq) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type SmenaUpdateResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
}

func (x *SmenaUpdateResp) Reset() {
	*x = SmenaUpdateResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaUpdateResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaUpdateResp) ProtoMessage() {}

func (x *SmenaUpdateResp) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaUpdateResp.ProtoReflect.Descriptor instead.
func (*SmenaUpdateResp) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{7}
}

func (x *SmenaUpdateResp) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

type SmenaDeleteResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
}

func (x *SmenaDeleteResp) Reset() {
	*x = SmenaDeleteResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaDeleteResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaDeleteResp) ProtoMessage() {}

func (x *SmenaDeleteResp) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaDeleteResp.ProtoReflect.Descriptor instead.
func (*SmenaDeleteResp) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{8}
}

func (x *SmenaDeleteResp) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

type SmenaCheckReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StaffId  string `protobuf:"bytes,1,opt,name=staff_id,json=staffId,proto3" json:"staff_id,omitempty"`
	BranchId string `protobuf:"bytes,2,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
}

func (x *SmenaCheckReq) Reset() {
	*x = SmenaCheckReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaCheckReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaCheckReq) ProtoMessage() {}

func (x *SmenaCheckReq) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaCheckReq.ProtoReflect.Descriptor instead.
func (*SmenaCheckReq) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{9}
}

func (x *SmenaCheckReq) GetStaffId() string {
	if x != nil {
		return x.StaffId
	}
	return ""
}

func (x *SmenaCheckReq) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

type SmenaCheckResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
}

func (x *SmenaCheckResp) Reset() {
	*x = SmenaCheckResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[10]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaCheckResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaCheckResp) ProtoMessage() {}

func (x *SmenaCheckResp) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[10]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaCheckResp.ProtoReflect.Descriptor instead.
func (*SmenaCheckResp) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{10}
}

func (x *SmenaCheckResp) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

var File_smena_proto protoreflect.FileDescriptor

var file_smena_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x73,
	0x61, 0x6c, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22, 0xc8, 0x01, 0x0a, 0x05,
	0x53, 0x6d, 0x65, 0x6e, 0x61, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f, 0x69,
	0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49, 0x64,
	0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1f, 0x0a,
	0x0b, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x02, 0x52, 0x0a, 0x73, 0x61, 0x6c, 0x65, 0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1f, 0x0a, 0x0b, 0x66, 0x69, 0x6e, 0x69, 0x73, 0x68, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x66, 0x69, 0x6e, 0x69,
	0x73, 0x68, 0x65, 0x64, 0x41, 0x74, 0x22, 0x81, 0x01, 0x0a, 0x0e, 0x53, 0x6d, 0x65, 0x6e, 0x61,
	0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x52, 0x65, 0x71, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61,
	0x66, 0x66, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61,
	0x66, 0x66, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69,
	0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49,
	0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0a, 0x73, 0x61, 0x6c, 0x65, 0x41, 0x6d, 0x6f, 0x75,
	0x6e, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x23, 0x0a, 0x0f, 0x53, 0x6d,
	0x65, 0x6e, 0x61, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x12, 0x10, 0x0a,
	0x03, 0x6d, 0x73, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x73, 0x67, 0x22,
	0x3b, 0x0a, 0x0f, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52,
	0x65, 0x71, 0x12, 0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x04, 0x70, 0x61, 0x67, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x22, 0x55, 0x0a, 0x10,
	0x53, 0x6d, 0x65, 0x6e, 0x61, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70,
	0x12, 0x2b, 0x0a, 0x06, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x13, 0x2e, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e,
	0x53, 0x6d, 0x65, 0x6e, 0x61, 0x52, 0x06, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x73, 0x12, 0x14, 0x0a,
	0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f,
	0x75, 0x6e, 0x74, 0x22, 0x1c, 0x0a, 0x0a, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x49, 0x64, 0x52, 0x65,
	0x71, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x22, 0x91, 0x01, 0x0a, 0x0e, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x55, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x52, 0x65, 0x71, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49, 0x64, 0x12,
	0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b,
	0x73, 0x61, 0x6c, 0x65, 0x5f, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x02, 0x52, 0x0a, 0x73, 0x61, 0x6c, 0x65, 0x41, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x16, 0x0a,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73,
	0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x23, 0x0a, 0x0f, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x12, 0x10, 0x0a, 0x03, 0x6d, 0x73, 0x67, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x73, 0x67, 0x22, 0x23, 0x0a, 0x0f, 0x53, 0x6d,
	0x65, 0x6e, 0x61, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x52, 0x65, 0x73, 0x70, 0x12, 0x10, 0x0a,
	0x03, 0x6d, 0x73, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x73, 0x67, 0x22,
	0x47, 0x0a, 0x0d, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x52, 0x65, 0x71,
	0x12, 0x19, 0x0a, 0x08, 0x73, 0x74, 0x61, 0x66, 0x66, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x73, 0x74, 0x61, 0x66, 0x66, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x22, 0x22, 0x0a, 0x0e, 0x53, 0x6d, 0x65, 0x6e,
	0x61, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x52, 0x65, 0x73, 0x70, 0x12, 0x10, 0x0a, 0x03, 0x6d, 0x73,
	0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x73, 0x67, 0x42, 0x17, 0x5a, 0x15,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x61, 0x6c, 0x65, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_smena_proto_rawDescOnce sync.Once
	file_smena_proto_rawDescData = file_smena_proto_rawDesc
)

func file_smena_proto_rawDescGZIP() []byte {
	file_smena_proto_rawDescOnce.Do(func() {
		file_smena_proto_rawDescData = protoimpl.X.CompressGZIP(file_smena_proto_rawDescData)
	})
	return file_smena_proto_rawDescData
}

var file_smena_proto_msgTypes = make([]protoimpl.MessageInfo, 11)
var file_smena_proto_goTypes = []interface{}{
	(*Smena)(nil),            // 0: sale_service.Smena
	(*SmenaCreateReq)(nil),   // 1: sale_service.SmenaCreateReq
	(*SmenaCreateResp)(nil),  // 2: sale_service.SmenaCreateResp
	(*SmenaGetListReq)(nil),  // 3: sale_service.SmenaGetListReq
	(*SmenaGetListResp)(nil), // 4: sale_service.SmenaGetListResp
	(*SmenaIdReq)(nil),       // 5: sale_service.SmenaIdReq
	(*SmenaUpdateReq)(nil),   // 6: sale_service.SmenaUpdateReq
	(*SmenaUpdateResp)(nil),  // 7: sale_service.SmenaUpdateResp
	(*SmenaDeleteResp)(nil),  // 8: sale_service.SmenaDeleteResp
	(*SmenaCheckReq)(nil),    // 9: sale_service.SmenaCheckReq
	(*SmenaCheckResp)(nil),   // 10: sale_service.SmenaCheckResp
}
var file_smena_proto_depIdxs = []int32{
	0, // 0: sale_service.SmenaGetListResp.smenas:type_name -> sale_service.Smena
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_smena_proto_init() }
func file_smena_proto_init() {
	if File_smena_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_smena_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Smena); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaCreateReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaCreateResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaGetListReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaGetListResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaIdReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaUpdateReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaUpdateResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaDeleteResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaCheckReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[10].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaCheckResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_smena_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   11,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_smena_proto_goTypes,
		DependencyIndexes: file_smena_proto_depIdxs,
		MessageInfos:      file_smena_proto_msgTypes,
	}.Build()
	File_smena_proto = out.File
	file_smena_proto_rawDesc = nil
	file_smena_proto_goTypes = nil
	file_smena_proto_depIdxs = nil
}
